from graphene import ObjectType, String, Int, Schema, Field, List
from model.schoolCodeTable import SchoolCodeTable
from mutation.login import Login

from service.schoolCodeTableService import SchoolCodeTableService

class Query(ObjectType):

  get_all = Field(List(SchoolCodeTable))
  get_all_have_records = Field(List(SchoolCodeTable))
  get_by_entity_id = Field(SchoolCodeTable, entity_id = String(required = True))

  def resolve_get_by_entity_id(self, info, entity_id):
    s = SchoolCodeTableService()
    return s.get_by_entity_id(entity_id = entity_id)

  def resolve_get_all_have_records(self, info):
    s = SchoolCodeTableService()
    return s.get_all(ignoe_empty_entity_id = True)

  def resolve_get_all(self, info):
    s = SchoolCodeTableService()
    return s.get_all()

class Mutation(ObjectType):
  login = Login.Field()
  pass

schema = Schema(query = Query, mutation = Mutation)