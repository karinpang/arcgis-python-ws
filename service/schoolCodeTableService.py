from service.dbService import DbService, DbObj
from model.schoolCodeTable import SchoolCodeTable

db_host = "db_arcgis"

class SchoolCodeTableService:

  __singleton = None

  def __init__(self):
    self.db = DbService()

  def __new__(cls, *args, **kwargs):
    if cls.__singleton == None:
        cls.__singleton = super(SchoolCodeTableService, cls).__new__(cls)
    return cls.__singleton

  def get_by_entity_id(self, entity_id: str):
    sql = "SELECT id, school_code, entity_id, url, consumer_url, create_date FROM `school_code_table` WHERE entity_id = %s"
    db = self.db.get_db(db_type = db_host)
    cursor = db.get_cursor()
    sql_result = cursor.execute(sql, (entity_id))
    if sql_result:
      return cursor.fetchone()
    else:
      return None

  def get_all(self, ignoe_empty_entity_id = False):
    extra_sql = " ORDER BY school_code ASC"
    if ignoe_empty_entity_id:
      extra_sql = " WHERE entity_id <> \"\" ORDER BY entity_id ASC"

    sql = "SELECT id, school_code, entity_id, url, consumer_url, create_date FROM `school_code_table`" + extra_sql
    db = self.db.get_db(db_type = db_host)
    cursor = db.get_cursor()
    sql_result = cursor.execute(sql)
    if sql_result:
      v = []
      for row in cursor.fetchall():
        v.append(row)
      return v
    else:
      return None