import configFile

import pymysql.cursors
from service.configService import ConfigService

from typing import TypeVar

config_tags = ['host', 'port', 'dbname', 'username', 'password']

class DbObj:
  def __init__(self, conn, cursor):
    self.conn = conn
    self.cursor = cursor

  def get_conn(self):
    return self.conn

  def get_cursor(self):
    return self.cursor

  def __del__(self):
    try:
      self.conn.close()
      self.cursor.close()
    except Exception as e:
      print(e)
  pass

DbObjType = TypeVar("DbObjType",bound=DbObj)

class DbService:

  __singleton = None

  def __init__(self):
    self.config_service = ConfigService(configFile.config_file)
    self.dbs = {}
    pass

  def __new__(cls, *args, **kwargs):
    if cls.__singleton == None:
        cls.__singleton = super(DbService, cls).__new__(cls)
    return cls.__singleton

  def create_connection(self, db_type: str) -> DbObjType:
    db_config = {}
    for t in config_tags:
      db_config[t] = self.config_service.get_config(type=db_type, tag=t)
    conn = pymysql.connect(user = db_config["username"], passwd = db_config["password"], host = db_config["host"], port=int(db_config["port"]), db=db_config["dbname"], cursorclass = pymysql.cursors.DictCursor)
    c = conn.cursor()

    dbObj = DbObj(conn = conn, cursor = c)
    return dbObj

  def get_db(self, db_type: str):
    try:
      if (db_type):
        # Check if already exists in dbs
        if (db_type in self.dbs):
          return self.dbs[db_type]
        else:
          self.dbs[db_type] = self.create_connection(db_type = db_type)
          return self.dbs[db_type]
    except Exception as e:
      print("ERROR in getDb", e)

  def execute(self, db_type: str, sql: str, params = None):
    try:
      db = self.get_db(db_type)
      if db:
        return db.execute(sql, params)
      else:
        return None
    except Exception as e:
      print("ERROR in execute", e)
      return None