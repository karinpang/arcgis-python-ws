import configparser

class ConfigService:

  __singleton = None

  def __init__(self, filename):
    self.config = configparser.SafeConfigParser()
    self.config.read(filename)
    pass

  def __new__(cls, *args, **kwargs):
    if cls.__singleton == None:
        cls.__singleton = super(ConfigService, cls).__new__(cls)
    return cls.__singleton

  def get_config(self, type, tag):
    return self.config.get(type, tag)

  pass