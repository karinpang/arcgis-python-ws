import requests
import json

headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

class HttpRequestService:

  __singleton = None

  def __new__(cls, *args, **kwargs):
    if cls.__singleton == None:
        cls.__singleton = super(HttpRequestService, cls).__new__(cls)
    return cls.__singleton

  def post(self, url, data):
    try:
      data_string = json.dumps(data)
      response = requests.post(url, headers = headers, data = data_string)
      response_json = json.loads(response.text)
      return response_json
    except Exception as e:
      print(e)
      return None

  pass