from service.configService import ConfigService
from service.httpRequestService import HttpRequestService
import configFile

jwt_create_mutation = """mutation ($secret: String!, $alias: String, $password: String) {
  create(secret: $secret, alias: $alias, password: $password) {
    jwt
    expire_at
  }
}"""

def return_response(err_code: int = 0, err_msg: str = ""):
  return {
    "error_code": err_code,
    "error_message": err_msg
  }

class LoginService:

  __singleton = None

  def __init__(self):
    self.config_service = ConfigService(configFile.config_file)
    self.http_request_service = HttpRequestService()
    self.jwt_url = self.config_service.get_config(type='jwt', tag='url')
    self.jwt_secret = self.config_service.get_config(type='jwt', tag='secret')
    self.valid_users = self.config_service.get_config(type='login', tag='valid_user').split(',')
    pass

  def __new__(cls, *args, **kwargs):
    if cls.__singleton == None:
        cls.__singleton = super(LoginService, cls).__new__(cls)
    return cls.__singleton

  def login_auth(self, username: str, password: str):
    if not username or not password:
      print("Empty Username / Password")
      return return_response(err_code=1, err_msg="Empty Username / Password")
    elif not self.check_valid_user_name(username):
      print("Login Error: Unauthorized")
      return return_response(err_code=5, err_msg="You are not authorized to access this page")
    else:
      return self.login_jwt_by_username_and_password(username, password)

  def check_valid_user_name(self, username: str):
    return username in self.valid_users

  def login_jwt_by_username_and_password(self, username: str, password: str):
    request = {
      "query": jwt_create_mutation,
      "variables": {
        "secret": self.jwt_secret,
        "alias": username,
        "password": password
      }
    }
    response = self.http_request_service.post(self.jwt_url, request)
    if (response is not None):
      if ('data' in response and response["data"] is not None and
          'create' in response["data"] and response["data"]["create"] is not None and
          'jwt' in response["data"]["create"] and response["data"]["create"]["jwt"] is not None):
        print("{} Login Success, get JWT = {}".format(username, response["data"]["create"]["jwt"]))
        return return_response()
      else:
        print("{} Login Error: JWT login return failure".format(username))
        return return_response(err_code=3, err_msg="Login failure, check username and password")
    else:
      print("{} Login Error: JWT login return failure".format(username))
      return return_response(err_code=4, err_msg="Login failure, check username and password")

  pass