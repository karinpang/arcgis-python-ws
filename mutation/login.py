from graphene import Mutation, ID, Int, String
from service import loginService

class Login(Mutation):
  err_code = Int()
  err_msg = String()

  class Arguments:
    username = String()
    password = String()

  def mutate(self, info, username, password):
    ls = loginService.LoginService()
    login_result = ls.login_auth(username = username, password = password)
    return Login(
      err_code = login_result["error_code"],
      err_msg = login_result["error_message"]
    )
  pass
pass