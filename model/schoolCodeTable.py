from graphene import ObjectType, String, Int, Schema, ID

class SchoolCodeTable(ObjectType):
  id = ID()
  school_code = String()
  entity_id = String()
  url = String()
  consumer_url = String()
  create_date = String()